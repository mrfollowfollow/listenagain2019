var spinRotation = 0.001;

jQuery(document).ready(function(){

    console.log("hello")

    // participants

    var participants = $.getJSON("participants.json", function() {
      console.log("success");
      $.each(participants['responseJSON'], function(key, value){
        $("#participants").append("<div class='participant block' id=" + value["Name"] + "><p>" + value["Name"] + "</p></div>");
      });
    });

    // $.getJSON("https://api.myjson.com/bins/7i7ua", function(json) {
    //     console.log(json['Lil Data']); // this will show the info it in firebug console
    //     $.each(json, function(key, value){
    //       $("#participants").append("<div class='participant block'><p>" + field['value']['Involved In'][1] + "</p></div>");
    //     });
    // });

    // schedule

    var schedule = $.getJSON("schedule.json", function() {
      console.log("success");
      $.each(schedule['responseJSON']['events'], function(key, value){
        $("#events").append("<div class='event block'><p>" + value['Name'] + ", " + value['Date'] + "</p></div>");
      });
      findStuff();
    });

    console.log("finished");

  function findStuff(){
    $( ".participant" )
      .click(function(e) {
        var block = $( '#participantInfoText');
        var showIt = $( '#participantInfo');
        var hideIt = $('#events')
        var part = participants['responseJSON'].find(x => x.Name === $(this).text());
        block.html(
          "<img  src='images/"+ part["Image"] + "''  class='image'/><h4>" + part['Name'] + "</h4>" + "<p>" + part['Info'] + "</p>" + "<p>Appearing at: " + part['Involved In']+ "</p>"
        );
        hideIt.hide();
        showIt.show();
        // block.addClass('showInfo');

      });
      // .mouseout(function(e) {
      //   $( '#participantInfo' ).removeClass('showInfo');
      // });

      $( ".event" )
        .click(function(e) {
          var block = $( '#eventInfoText' )
          var showIt = $( '#eventInfo' )
          var hideIt = $('#participants')
          var eventName = $(this).text().split(",")[0];
          var eventt = schedule['responseJSON']['events'].find(x => x.Name === eventName)
          block.html(
            "<img src='images/" + eventt["Image"] + " ' class='image'/></h4>" + "<p class='local'>Location: " + eventt['Location'] + "<div id='map'><iframe src=" + eventt['Map'] + " frameborder='0' style='border:0' allowfullscreen></iframe></div>" + "</p>" + "<p>Date: " + eventt['Date'] + ", " + eventt['Time'] + "</p>" + "<p>" + eventt['Info'] + "</p>" + "<p>Artists: " + eventt['Artists'] + "</p>" + "<p>" + eventt['Type'] + "</p>"
          );
          hideIt.hide();
          showIt.show();
          $( ".local" )
            .click(function(e) {
              console.log("click");
              $( "#map" ).toggleClass( 'show' );
            }
          )
          // block.addClass('showInfo');
        });
        // .mouseout(function(e) {
        //   $( '#eventInfo' ).removeClass('showInfo');
        // });



      $("#closeParticipant").click(function(e) {
        $(this).parent().hide();
        $('#events').show();
      });

      $("#closeEvent").click(function(e) {
        console.log("close");
        $(this).parent().hide();
        $('#participants').show();
      });

        // $( 'html' ).mousemove(function(e) {
        //   $('#participantInfo').css({'top':e.pageY+150,'left':e.pageX+50});
        //   $('#eventInfo').css({'top':e.pageY-50,'left':e.pageX+50});
        // });


    };

    function strobee(){
      $("#200bpm").addClass('strobe')
    }

    function unstrobee(){
      $("#200bpm").removeClass('strobe')
    }

    $("#add").click(function(e) {
      var number = parseInt($("#bpm").text());
      number = number + 1
      if (number > 127){
        strobee();
      };
      $("#bpm").text(number.toString());
      spinRotation = spinRotation + 0.01;
      console.log(spinRotation);
    });

    $("#minus").click(function(e) {
      var number = parseInt($("#bpm").text());
      number = number - 1
      $("#bpm").text(number.toString());
      spinRotation = spinRotation - 0.01;
      console.log("minus");
      console.log(spinRotation);
    });


});


// hover
