

// ------------------------------------------------
// 3D
// ------------------------------------------------

// get canvas
container = document.getElementById( 'container' );

// Create an empty scene
var scene = new THREE.Scene();

// Create a basic perspective camera
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 10;

// controls

// var controls = new THREE.OrbitControls( camera );

// Create a renderer with Antialiasing
var renderer = new THREE.WebGLRenderer({antialias:true});

// Configure renderer clear color
renderer.setClearColor("#000");

// Configure renderer size
console.log($(container).width())
renderer.setSize($(container).width(), $(container).height());
renderer.setSize( window.innerWidth, window.innerHeight );

// Append Renderer to DOM
document.body.appendChild( renderer.domElement );

// ------------------------------------------------
// FUN STARTS HERE
// ------------------------------------------------

// LIGHTS
scene.add( new THREE.HemisphereLight( 0x443333, 0x111122 ) );
var spotLight = new THREE.SpotLight( 0x963d3d, 2 );
spotLight.position.set( 0.5, 0, 1 );
spotLight.position.multiplyScalar( 700 );
scene.add( spotLight );
spotLight.castShadow = true;
spotLight.shadow.mapSize.width = 2048;
spotLight.shadow.mapSize.height = 2048;
spotLight.shadow.camera.near = 200;
spotLight.shadow.camera.far = 1500;
spotLight.shadow.camera.fov = 40;
spotLight.shadow.bias = - 0.005;

// MATERIALS

var mapHeight = new THREE.TextureLoader().load( "bumpIt.jpg" );


var sampleMaterial = new THREE.MeshPhongMaterial( {
					color: 0x000,
					// specular: 0x963d3d,
					specular: 0xa11814,
					shininess: 30,
					bumpMap: mapHeight,
					bumpScale: 2.5
				} );

// OBJECTS

var loader = new THREE.OBJLoader();

				// load a resource
				loader.load(
					// resource URL
					'ear.obj',
					// called when resource is loaded
					function ( object ) {
						console.log("loaded")
						window.sample = object
						object.position.set( -3, -1, -2 );
						object.rotation.set( 0, -1, 0 );
						// object.scale.x = 0.1
						// object.scale.y = 0.1
						// object.scale.z = 4
						object.scale.x = 0.1;
						object.scale.y = 0.1;
						object.scale.z = 0.1;
						object.castShadow = true;
						object.receiveShadow = true;
						object.traverse( function ( child ) {
								if ( child instanceof THREE.Mesh ) {
										child.material = sampleMaterial;
								}
						} );
						scene.add( object );
						// move_camera();
						// show the scene.
						// remove loading allow
						$("#loading").fadeOut()
					},
					// called when loading is in progresses
					function ( xhr ) {
						var percent = (Math.round(xhr.loaded / xhr.total * 100));
						console.log(  'BPM: ' + percent);
						$('#loadingBPM').text( percent );

					},
					// called when loading has errors
					function ( error ) {

						console.log( 'An error happened' );

					}
);

// Create a Cube Mesh with basic material
var geometry = new THREE.BoxGeometry( 6, 8, 6 );
// var material = new THREE.MeshBasicMaterial( { color: "#000000" } );
var cube = new THREE.Mesh( geometry, sampleMaterial );
cube.position.set(0,0,0);

// Add cube to Scene
// scene.add( cube );

// var controls = new THREE.OrbitControls( camera, renderer.domElement );
// controls.target.copy( cube.position );
// controls.update();

// Render Loop
var render = function () {
  requestAnimationFrame( render );

  sample.rotation.x += 0.0001;
	sample.rotation.y += spinRotation;

	// cube.rotation.x += 0.0001;
	// cube.rotation.y += 0.001;

	spotLight.rotation.x += 1;
	// spotLight.rotation.y += 0.1;


  // Render the scene
  renderer.render(scene, camera);
};

container.appendChild(renderer.domElement);
render();
